<!DOCTYPE html>
<?php
include "../../koneksi.php";;
?>
<html>
<head>
	<title>Buku Panduan</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  	<link href="css/bootstrap.min.css" rel="stylesheet">
  	<link rel="stylesheet" type="text/css" href="">
  	<script type="text/javascript" src="js/jquery.min.js"></script>
  	<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-default navbar-static-top" >
<div class="container">
  <a class="navbar-brand" href="#"><b>Manual Book</b> <small>How to use ?</small></a>
  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav">
      <li class="active"><a href="a.php">Home</a></li>
  </div>
  </div>
</nav>
<div class="container">
	<div class="panel panel-default">
  		<div class="panel-heading"><b>Login</b></div>
  			<div class="panel-body">
				Sebelum login untuk meminjam barang kita perlu masuk kedalam aplikasi terlebih dahulu dengan link <b>http://ujikommaudy.sknc.site/KOKOM/KOKOM/login/login.php</b>, setelah masuk ke dalam aplikasi tersebut anda baru bisa login menggunakan Username dan Password yang sudah tertera pada tabel diatas.
				Ketika anda akan masuk sebagai admin atau operator maka anda akan lansung memasukan username dan password lalu klik login, tetapi jika anda masuk sebagai Peminjam(user) anda harus mengklik <b>Login Sebagai Pegawai !</b> dan hanya memasukan NIP saja.
				Ketika username dan password sudah benar baru anda bisa masuk untuk meminjam barang. <br>
				Username : admin , Password : admin.<br>
				Username : operator , Password : operator<br>
				Masukan Nip : 1234 (untuk user)<br>
			<p align="center"><img src="loginulang.png"><br><i>Gambar 0.1 Login</i></p>
  		</div>
	</div>
	<div class="panel panel-default">
  		<div class="panel-heading"><b>Admin</b></div>
  			<div class="panel-body">
				Di dalam Admin terdapat Fitur-fitur untuk meminjam barang, melihat dan mengembalikan barang, dan membackup database.<br>
				Fitur- Fitur : <br>
				1. Data Inventaris, Data Jenis, Data Ruang <br>
				 Di dalam fitur ini anda bisa melihan menambah dan mengedit isi yang terdapat pada fitur ini<br><br>
				<p align="center"><img src="inven.png"><br><i>Gambar 0.1 Inventaris</i></p><br>
				2. Data Peminjaman dan Pengembalian User/Member<br>
				 Di dalam fitur ini anda bisa melihan siapa saja yang meminjam barang pada peminjaman user/member dan jika anda ingin mengembalikan barang tersebut tinggal klik button <b>KEMBALIKAN</b> dan barang tersebut akan kembali seperti semula.<br><br>
				<p align="center"><img src="pinjam.png"><br><i>Gambar 0.2 Data Peminjaman User</i></p><br>
				3. Data Peminjaman dan Pengembalian Operator<br>
				 Di dalam fitur ini anda bisa melihan siapa saja yang meminjam barang pada peminjaman user/member dan jika anda ingin mengembalikan barang tersebut tinggal klik button <b>KEMBALIKAN</b> dan barang tersebut akan kembali seperti semula.<br><br>
				<p align="center"><img src="kembali.png"><br><i>Gambar 0.3 Data Peminjaman Operator</i></p><br>
				4. Backup Database<br>
				 Di fitur anda dapat membackup seluruh database dan anda juga bisa mengexportnya satu persatu<br><br>
				<p align="center"><img src="database.png"><br><i>Gambar 0.4 Backup Database</i></p><br>
				5. Pengguna<br>
				 Di fitur anda dapat menambahkan data si user untuk dapat meminjam barag<br><br>
				<p align="center"><img src="pengguna.png"><br><i>Gambar 0.5 Pengguna</i></p><br>

  		</div>
	</div>
	<div class="panel panel-default">
  		<div class="panel-heading"><b>Operator</b></div>
  			<div class="panel-body">
  				Di dalam Fitur Operator ini anda hanya dapat meminjam dan mengembalikan barang <br>
  				Fitur-fitur : <br>
				1. Peminjaman Operator<br>
				 Di fitur anda dapat meminjam barang<br><br>
				<p align="center"><img src="operator.png"><br><i>Gambar 1.1 Pinjam Operator</i></p><br>
				2. Pengembalian<br>
				 Di fitur anda dapat mengembalikan barang-barang yang dipinjam oleh user/member<br><br>
				<p align="center"><img src="kembali_operator.png"><br><i>Gambar 1.2 Pengembalian</i></p><br>
			</div>
  	</div>
  	<div class="panel panel-default">
  		<div class="panel-heading"><b>User/Member</b></div>
  			<div class="panel-body">
				1. Peminjaman User/Member<br>
  				Di User anda hanya bisa meminjam barang saja, ketika anda akan meminjam barang anda harus mengklik tombol pinjam dan memasukan jumlah barang yang akan anda pinjam selanjutnya pilih pinjam. Data tersebut akan masuk kedalam Admin <br><br>
				<p align="center"><img src="user.png"><br><i>Gambar 2.1 Pinjaman User/Member</i></p><br>
			</div>
  		</div>
</div>
</body>
</html>