-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 08, 2019 at 02:44 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom_maudy`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_peminjaman` int(11) NOT NULL,
  `id_peminjaman` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(20) NOT NULL,
  `status` enum('Dikembalikan','Belum Dikembalikan') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_peminjaman`, `id_peminjaman`, `id_inventaris`, `jumlah_pinjam`, `status`) VALUES
(27, 26, 43, 1, 'Dikembalikan'),
(28, 26, 39, 1, 'Dikembalikan'),
(29, 26, 44, 1, 'Dikembalikan'),
(30, 26, 44, 1, 'Dikembalikan'),
(31, 27, 46, 1, 'Dikembalikan'),
(32, 28, 52, 1, 'Dikembalikan'),
(33, 29, 52, 1, 'Dikembalikan'),
(34, 30, 52, 1, 'Dikembalikan'),
(35, 31, 52, 1, 'Dikembalikan'),
(36, 32, 52, 1, 'Dikembalikan'),
(37, 32, 52, 1, 'Dikembalikan'),
(38, 32, 52, 1, 'Dikembalikan'),
(39, 33, 52, 1, 'Dikembalikan'),
(40, 34, 52, 1, 'Dikembalikan'),
(41, 35, 52, 1, 'Dikembalikan'),
(42, 36, 52, 1, 'Dikembalikan'),
(43, 37, 46, 1, 'Dikembalikan'),
(44, 38, 46, 1, 'Dikembalikan'),
(45, 39, 45, 1, 'Dikembalikan'),
(46, 40, 53, 1, 'Dikembalikan'),
(47, 41, 53, 2, 'Dikembalikan'),
(48, 42, 53, 1, 'Dikembalikan'),
(49, 43, 46, 1, 'Dikembalikan'),
(50, 44, 46, 1, 'Dikembalikan'),
(51, 45, 44, 1, 'Dikembalikan'),
(52, 46, 44, 1, 'Dikembalikan');

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `kondisi` enum('Baik','Rusak Ringan','Rusak Berat','') NOT NULL,
  `foto` text NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(11) NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `foto`, `keterangan`, `jumlah`, `id_jenis`, `tanggal_register`, `id_ruang`, `kode_inventaris`, `id_petugas`) VALUES
(39, 'Mouse', 'Baik', 'mouse.jpg', '', 9, 19, '0000-00-00', 21, 'V0004', 1),
(43, 'Speaker ', 'Baik', 'speaker.jpg', '', 2, 19, '0000-00-00', 21, 'V0008', 1),
(44, 'CD', 'Baik', 'cd.jpg', '', 7, 19, '0000-00-00', 21, 'V0009', 1),
(45, 'Earphone', 'Baik', 'headset.png', '', 3, 19, '0000-00-00', 21, 'V0010', 1),
(46, 'Laptop Lenovo', 'Baik', 'lenovo.jpg', '', 33, 19, '0000-00-00', 21, 'V0011', 1),
(51, 'Kunci Inggris', 'Rusak Ringan', 'kunciinggris.jpg', '', 2, 20, '2019-03-02', 16, 'V0015', 1),
(52, 'Laptop Acer', 'Baik', 'acer.jpg', '', 26, 17, '2018-03-03', 18, 'V0016', 1),
(53, 'Kacamata Las', 'Baik', 'kacamatalas.jpg', '', 28, 21, '2017-09-08', 17, 'V0017', 1),
(54, 'Kamera', 'Baik', 'kamera.jpg', '', 3, 18, '2019-01-08', 24, 'V0018', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(11) NOT NULL,
  `keterangan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(17, 'Animasi', 'J0001', 'Ada'),
(18, 'BroadCasting', 'J0002', 'Ada'),
(19, 'Rekayasa Perangkat Lunak', 'J0003', 'Ada'),
(20, 'Teknik Kendaraan Ringan', 'J0004', 'Ada'),
(21, 'Teknik Pengelasan', 'J0005', 'Ada');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'operator'),
(3, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id_member` int(11) NOT NULL,
  `nama_member` varchar(50) NOT NULL,
  `nip` int(11) NOT NULL,
  `telp` int(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `status` varchar(200) NOT NULL,
  `user_key` int(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `nama_member`, `nip`, `telp`, `alamat`, `status`, `user_key`) VALUES
(1, 'Maudy Fitri  Fazriani', 1122, 896113969, 'Jln.Arde Laladon Indah', '', 0),
(2, 'Maudy Fazriani', 1234, 219634739, 'Ciomas', '', 0),
(3, 'Maudy Fitri', 738798739, 275872898, 'Laladon', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(20) NOT NULL,
  `id_member` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `id_inventaris`, `tanggal_pinjam`, `tanggal_kembali`, `status_peminjaman`, `id_member`, `id_petugas`) VALUES
(39, 45, '2019-04-06', '0000-00-00', 'Sedang Dipinjam', 1, 0),
(40, 53, '2019-04-07', '0000-00-00', 'Sedang Dipinjam', 1, 0),
(42, 53, '2019-04-07', '0000-00-00', 'Sedang Dipinjam', 2, 0),
(44, 46, '2019-04-07', '0000-00-00', 'Sedang Dipinjam', 0, 2),
(45, 44, '2019-04-08', '0000-00-00', 'Sedang Dipinjam', 2, 0),
(46, 44, '2019-04-08', '0000-00-00', 'Sedang Dipinjam', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `email` varchar(35) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  `status` int(200) NOT NULL,
  `user_key` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `email`, `password`, `nama_petugas`, `id_level`, `status`, `user_key`) VALUES
(1, 'admin', 'maudyfazriani01@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'maudy fitri', 1, 1, 'efad8be464ac484880dcb00a2f7746d85ca86704696ca'),
(2, 'operator', 'maudyfazriani01@gmail.com', '4b583376b2767b923c3e1da60d10de59', 'maudy fitri', 2, 0, '0'),
(3, 'user', 'maudyfazriani01@gmail.com', 'user', 'maudy fitri', 3, 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(11) NOT NULL,
  `keterangan` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(16, 'Bengkel tkr', 'R0001', 'Dekat Studio Animasi'),
(17, 'Bengkel TPL', 'R0002', 'Dekat Tangga Sebelah Tata Usah'),
(18, 'Lab Animasi', 'R0003', 'Sebelah Ruang Produksi'),
(19, 'Lab Bahasa', 'R0004', 'Sebelah Tata Usaha'),
(20, 'Lab RPL 1', 'R0005', 'Dekat Tangga Sebelah Utara'),
(21, 'Lab RPL 2', 'R0006', 'Sebelah Lab RPL 1'),
(22, 'Ruang Guru', 'R0007', 'Sebelah Mushola'),
(23, 'Ruang Produksi', 'R0008', 'Sebelah Lab Animasi'),
(24, 'Studio Animasi', 'R0009', 'Dekat Bengkel TKR '),
(25, 'Tata Usaha', 'R0010', 'Sebelah Lab Bahasa'),
(26, 'Perpustakaan', 'R0011', 'Diatas Bengkel TPL');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_peminjaman`),
  ADD KEY `id_peminjaman` (`id_peminjaman`,`id_inventaris`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_jenis` (`id_jenis`,`id_ruang`,`id_petugas`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `id_petugas` (`id_member`),
  ADD KEY `id_petugas_2` (`id_petugas`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `id_level` (`id_level`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id_detail_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id_member` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
