<?php
include 'header.php';
?>
<body>
  <section id="container">
    <header class="header black-bg" style="background: #365c7d;">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right"></div>
      </div>
      <a href="#" class="logo"><b>VOS<span>IN !</span></b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="../login/logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse " style="background: #303b58;">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><img src="img/8.png" class="img-circle" width="80"></p>
          <h5 class="centered">Hy, User !</h5>
          <li>
            <a class="active" href="pinjam_user.php">
              <i class="fa fa-reply"></i>
              <span>Peminjaman</span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
    <section id="main-content">
      <section class="wrapper">
      <h3><i class="fa fa-angle-right"></i> Peminjaman Data</h3>
       <div class="container">
          <div class="row mt">
            <div class="col-lg-8">
              <?php
                   include "../koneksi.php";
                   $query = mysqli_query($koneksi, "SELECT * from inventaris");
                   while ($show = mysqli_fetch_array($query)){
              ?>
              <div class="col-lg-4 col-md-4 col-sm-4 mb">
                <div class="product-panel-2 pn"><br>
                    <h4><?php  echo $show['nama']; ?></h4>
                    <img src="../admin/img/<?=$show['foto'];?> " width="40%" height="40%">
                    <p>Stok:<?php echo $show['jumlah']; ?></p>
                    <button type="button" class="btn btn-small btn-theme04" data-toggle="modal" data-target="#pinjam<?=$show['id_inventaris'];?>"> PINJAM
                    </button>
                </div>
              </div>
<!-- Modal -->
<div class="modal fade" id="pinjam<?=$show['id_inventaris'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Peminjaman Barang</h4>
      </div>
      <div class="modal-body" align="center">
        <form class="form-horizontal" action="prosses_pinjam.php" method="POST">
          <input type="hidden" name="id_member" value="<?php echo $_SESSION['Member'];?>">
          <input type="hidden" name="id_inventaris" value="<?=$show ['id_inventaris'];?>">
            <div class="form-group">
              <label for="recipient-name" class="col-sm-2 control-label">Nama Peminjam</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="recipient-name" name="nama_member" value="<?php echo $_SESSION['nama_member'];?>" readonly>
              </div>
            </div>
            <div class="form-group">
              <label for="recipient-name" class="col-sm-2 control-label">Nama Barang</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" value="<?php echo $show['nama'];?>" readonly>
              </div>
            </div>
            <div class="form-group">
            <?php
            date_default_timezone_set('Asia/Jakarta');
            $date = date('d' . '-' . 'F' . '-' . 'Y');
             ?>
              <label for="recipient-name" class="col-sm-2 control-label">Tgl Pinjam</label>
              <div class="col-sm-10">
                <input type="text" disabled name="tanggal_pinjam" class="datepicker form-control" value="<?php echo $date; ?>">
                <input type="hidden" name="tanggal_pinjam" class="datepicker form-control" value="<?php echo $date; ?>">
              </div>
            </div>
            <div class="form-group">
              <label for="message-text" class="col-sm-2 control-label">Jumlah</label>
              <div class="col-sm-10">
                <input type="number" name="jumlah" class="form-control" max="<?php echo $show['nama'];?>" value="1" min="1">
              </div>
            </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary" name="submit" value="simpan"> Pinjam</button>
            </div>
        </form>
    </div>
  </div>
</div>
              <?php
                }
              ?>
            </div>
          </div>
        </div>
      </section>
    </section>
  </section>
<?php
include "footer.php";
?>
</body>

</html>
