<?php
include 'header.php';
?>
<body>
  <section id="container">
    <header class="header black-bg" style="background: #365c7d;">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right"></div>
      </div>
      <a href="#" class="logo"><b>VOS<span>IN !</span></b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="../login/logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse " style="background: #303b58;">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="img/8.png" class="img-circle" width="80"></a></p>
          <h5 class="centered">Admin !</h5>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-edit"></i>
              <span>Inventaris</span>
            </a>
            <ul class="sub">
              <li style="background: #303b58;"><a href="inventaris_admin.php">Data Inventaris</a></li>
              <li style="background: #303b58;"><a href="jenis_admin.php">Data Jenis</a></li>
              <li style="background: #303b58;"><a href="ruang_admin.php">Data Ruang</a></li>
            </ul>
          </li>
          <li>
            <a href="pinjam_admin.php">
              <i class="fa fa-reply"></i>
              <span>Data Peminjaman</span>
            </a>
          </li>
          <li>
            <a href="kembali_admin.php">
              <i class="fa fa-share"></i>
              <span>Data Pengembalian</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-cogs"></i>
              <span>Generate Laporan</span>
            </a>
            <ul class="sub">
              <li><a href="report_admin.php">Report</a></li>
              <li><a href="backup_database.php">Backup Database</a></li>
            </ul>
          </li>
          <li>
            <a href="pengguna_admin.php">
              <i class="fa fa-envelope"></i>
              <span>Pengguna </span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="container">
            <div class="col-md-10 col-sm-10 col-xs-10">
              <h3><i class="fa fa-angle-right"></i> Tambah Data Inventaris</h3>
              <div class="x_content"><br>
                <form action="prossestambah_inventaris.php" method="post" class="form-horizontal form-label-left" novalidate>
                  <div class="form-group">
                    <label for="nama">Nama Barang</label>
                    <input type="text" class="form-control" id="nama" placeholder="Masukan Barang" name="nama" required>
                  </div>
                  <div class="form-group">
                    <label for="kondisi">Kondisi</label>
                    <select class="form-control" name="kondisi">
                      <option value="" disabled selected>Pilih Kondisi</option>
                      <option value="Baik">Baik</option> 
                      <option value="Rusak Ringan">Rusak Ringan</option> 
                      <option value="Rusak Berat">Rusak Berat</option> 
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="jumlah">Jumlah</label>
                    <input type="number" class="form-control" id="jumlah" placeholder="Masukan Jumlah" name="jumlah" required>
                  </div>
                  <div class="form-group">
                    <label for="id_jenis">Nama Jurusan</label>
                    <select class="form-control" name="id_jenis">
                      <option value="" disable seleted>--Pilih Nama Jenis--</option>
                      <?php
                      include "../koneksi.php";
                      $query = mysqli_query($koneksi, "SELECT * from jenis");
                      while ($data=mysqli_fetch_array($query)) {
                        ?>
                        <option value="<?php echo $data['id_jenis'];?>"><?php echo $data['nama_jenis'];?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="tanggal_register">Tanggal Register</label>
                      <input class="form-control" type="date" name="tanggal_register" placeholder="Masukan Tanggal" required="">
                    </div>
                    <div class="form-group">
                      <label for="id_ruang">Nama Ruang</label>
                      <select class="form-control" name="id_ruang">
                        <option value="" disable seleted>--Pilih Ruangan--</option>
                        <?php
                        include "../koneksi.php";
                        $select = mysqli_query($koneksi, "SELECT * from ruang");
                        while ($isi=mysqli_fetch_array($select)) {
                          ?>
                          <option value="<?php echo $isi['id_ruang'];?>"><?php echo $isi['nama_ruang'];?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Kode Inventaris</label>

                        <?php
                        $koneksi = mysqli_connect("localhost","root","","ujikom_maudy");
                        $cari_kd = mysqli_query($koneksi, "SELECT max(kode_inventaris) as kode from inventaris");
                        //besar atau kode yang baru masuk
                        $tm_cari = mysqli_fetch_array($cari_kd);
                        $kode    = substr($tm_cari['kode'],1,4);
                        $tambah  = $kode+1;
                        if ($tambah<10){
                          $kode_inventaris = "V000".$tambah;
                        }else{
                          $kode_inventaris = "V00".$tambah;
                        }
                        ?>
                        <input class="form-control" name="kode_inventaris" value="<?php echo $kode_inventaris; ?>" type="text" placeholder="Masukan Kode Inventaris" required="">
                      </div>    
                      <div class="form-group">
                        <label for="id_petugas">Petugas</label>
                        <select name="id_petugas" class="form-control" required>
                          <?php
                            include "../koneksi.php";
                            $sql = mysqli_query($koneksi, "SELECT * from petugas");
                            while ($data=mysqli_fetch_array($sql)) {
                              if ($data['id_petugas']==$tampil['id_petugas']){
                                $seleted="seleted";
                              } else {
                                $seleted=="";
                              }
                              ?>

                              <option value="<?php echo $data['id_petugas'];?> <?php echo $seleted ?> <?php echo $data['username'];?>"></option>
                              <?php
                            }
                          ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="foto">Foto</label>
                        <input type="file" name="foto" class="form-control" placeholder="Foto..." required>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <a href="inven_admin.php" class="btn btn-danger" type="button">Cancel</a>
                          <button type="submit" class="btn btn-info" name="simpan">Submit</button>
                        </div>
                      </div>
                    </form>

                  </div>
                </div>
              </div>
            </div>
          </section>
        </section>
      </section>
      <?php
      include "footer.php";
      ?>
    </body>

    </html>
