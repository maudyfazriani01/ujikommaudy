<?php
include "header.php";
?>
<body>
  <section id="container">
    <header class="header black-bg" style="background: #365c7d;">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right"></div>
      </div>
      <a href="#" class="logo"><b>VOS<span>IN !</span></b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
           <li><a class="logout" href="../login/logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse " style="background: #303b58;">
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="img/8.png" class="img-circle" width="80"></a></p>
          <h5 class="centered">Admin !</h5>
          <li class="sub-menu">
            <a  href="javascript:;">
              <i class="fa fa-edit"></i>
              <span>Inventaris</span>
              </a>
            <ul class="sub">
              <li style="background: #303b58;"><a href="inventaris_admin.php">Data Inventaris</a></li>
              <li style="background: #303b58;"><a href="jenis_admin.php">Data Jenis</a></li>
              <li style="background: #303b58;"><a href="ruang_admin.php">Data Ruang</a></li>
            </ul>
          </li>
          <li>
                <a href="pinjam_admin.php">
                  <i class="fa fa-reply"></i>
                  <span>Data Peminjaman</span>
                </a>
              </li>
          <li>
            <a class="active" href="kembali_admin.php">
              <i class="fa fa-share"></i>
              <span>Data Pengembalian</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-cogs"></i>
              <span>Backup Database</span>
              </a>
            <ul class="sub">
              <li style="background: #303b58;"><a href="backup_database.php">Backup Database</a></li>
            </ul>
          </li>
          <li>
            <a href="pengguna_admin.php">
              <i class="fa fa-users"></i>
              <span>Pengguna </span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
    <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Data Peminjaman & Pengembalian Operator</h3>
        <div class="row mb">
          <div class="content-panel">
            <div class="container">
              <div class="adv-table">
              <a href="lap_pengembalian.php"><button class="btn btn-info"><i class="fa fa-plus"></i>Export PDF</button></a>
              <a href="export_excel_pengembalian.php"><button class="btn btn-info"><i class="fa fa-plus"></i>Export Excel</button></a><br/><br/>
              <table class="display table table-bordered" id="hidden-table-info">
                <thead>
                  <tr align="center">
                    <td><b>No</b></td>
                    <td><b>Nama Peminjaman</b></td>
                    <td><b>Nama Barang</b></td>
                    <td><b>Tanggal Pinjam</b></td>
                    <td><b>Tanggal Kembali</b></td>
                    <td><b>Jumlah</b></td>
                    <td><b>Status Peminjaman</b></td>
                    <td><b>Option</b></td>
                  </tr>
                </thead>
               <tbody>
            <?php
                    include "../koneksi.php";
                    $no=1;
                    $sql = mysqli_query($koneksi, "SELECT * from peminjaman p JOIN detail_pinjam d ON p.id_peminjaman=d.id_peminjaman JOIN petugas t ON p.id_petugas=t.id_petugas JOIN inventaris i ON d.id_inventaris=i.id_inventaris");
                    while ($s = mysqli_fetch_array($sql)){
            ?>
                <tr align="center">
                    <td height="42"><?php echo $no++;?></td>
                    <td><?=$s['username'];?></td>
                    <td><?=$s['nama'];?></td>
                    <td><?=$s['tanggal_pinjam'];?></td>
                    <td><?=$s['tanggal_kembali'];?></td>
                    <td><?=$s['jumlah_pinjam'];?></td>
                    <td><?=$s['status_peminjaman'];?></td>
                    <?php
                    if ($s['status_peminjaman']=='Sedang Dipinjam') {
                    ?>
                    <td>
                      <a href="prosseskembali_admin.php?id_peminjaman=<?=$s['id_peminjaman']?>&id_inventaris=<?=$s['id_inventaris']?>&jumlah=<?=$s['jumlah_pinjam']?>">
                    <button class="btn btn-info">Kembalikan</button></a></td>
               
                <?php
                }else{
                  echo "-"
                
                ?>
                <?php
                }
                ?>
                 </tr>
                 <?php
               }
               ?>
            </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      </section>
    </section>
        <footer class="site-footer">
      <div class="text-center">
        <p>
           &copy; Copyrights <strong>SMKN 1 CIOMAS</strong>
        </p>
        <div class="credits">
          <!--
            You are NOT allowed to delete the credit link to TemplateMag with free version.
            You can delete the credit link only if you bought the pro version.
            Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/dashio-bootstrap-admin-template/
            Licensing information: https://templatemag.com/license/
          -->
          JL. RAYA LALADON INDAH
        </div>
        <div class="col l4 s12">
            <p class="grey-text text-lighten-4">
            <a href=https://www.facebook.com/maudy.fzrni target='_blank'><img alt='Facebook' src='img/facebook.png' width="40px" title='Facebook'/></a>
            <a href=https://www.instagram.com/dymawfzrni/?hl=en target='_blank'><img alt='Instagram' src='img/instagram.png' width="40px" title='Instagram'/></a>
            <a href=https://twitter.com/kim_djimaw target='_blank'><img alt='Twitter' src='img/twitter.png' width="40px" title='Twitter'/></a>
          </div>
        <a href="form_validation.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript">
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
      sOut += '<tr><td>Rendering engine:</td><td>' + aData[1] + ' ' + aData[4] + '</td></tr>';
      sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
      sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
      nCloneTd.innerHTML = '<img src="lib/advanced-datatable/images/details_open.png">';
      nCloneTd.className = "center";

      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = "lib/advanced-datatable/media/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = "lib/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
  </script>
</body>

</html>
