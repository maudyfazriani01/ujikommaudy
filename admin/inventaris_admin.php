<?php
include 'header.php';
?>
<body>
  <section id="container">
    <header class="header black-bg" style="background: #365c7d;">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right"></div>
      </div>
      <a href="#" class="logo"><b>VOS<span>IN !</span></b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="../login/logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse " style="background: #303b58;">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="img/8.png" class="img-circle" width="80"></a></p>
          <h5 class="centered">Admin !</h5>
          <li class="sub-menu">
            <a class="active" href="javascript:;">
              <i class="fa fa-edit"></i>
              <span>Inventaris</span>
              </a>
            <ul class="sub">
              <li class="active" style="background: #303b58;"><a href="Inventaris_admin.php">Data Inventaris</a></li>
              <li style="background: #303b58;"><a href="jenis_admin.php">Data Jenis</a></li>
              <li style="background: #303b58;"><a href="ruang_admin.php">Data Ruang</a></li>
            </ul>
          </li>
      <li>
            <a href="pinjam_admin.php">
              <i class="fa fa-reply"></i>
              <span>Data Peminjaman</span>
            </a>
          </li>
      <li>
            <a href="kembali_admin.php">
              <i class="fa fa-share"></i>
              <span>Data Pengembalian</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-cogs"></i>
              <span>Backup Database</span>
              </a>
            <ul class="sub">
              <li><a  style="background: #303b58;" href="backup_database.php">Backup Database</a></li>
            </ul>
          </li>
          <li>
            <a href="pengguna_admin.php">
              <i class="fa fa-users"></i>
              <span>Pengguna </span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
    <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Data Inventaris</h3>
        <div class="row mt">
          <div class="col-lg-12">
            <div class="row">
              <div class="col-md-4 col-sm-4 mb">
                <div class="grey-panel pn donut-chart">
                  <div class="grey-header">
                  <h5>Silahkan Klik</h5>
                  </div>
                  <a href="inven_admin.php?jurusan=animasi"><img src="img/1.jpg" style="height: 165px; width: 230px;"></a>
                  <div class="grey-footer">
                    <h5> Animasi</h5>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-4 mb">
                <div class="grey-panel pn donut-chart">
                  <div class="grey-header">
                  <h5>Silahkan Klik</h5>
                  </div>
                  <a href="inven_admin.php?jurusan=broadcasting"><img src="img/5.png" style="height: 165px; width: 250px;"></a>
                  <div class="grey-footer">
                    <h5> BroadCasting</h5>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-4 mb">
                <div class="grey-panel pn donut-chart">
                  <div class="grey-header">
                  <h5>Silahkan Klik</h5>
                  </div>
                  <a href="inven_admin.php?jurusan=rekayasa perangkat lunak"><img src="img/logo.jpg" style="height: 165px; width: 270px;"></a>
                  <div class="grey-footer">
                    <h5> Rekayasa Perangkat Lunak</h5>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 col-sm-4 mb">
                <div class="grey-panel pn donut-chart">
                  <div class="grey-header">
                  <h5>Silahkan Klik</h5>
                  </div>
                  <a href="inven_admin.php?jurusan=teknik kendaraan ringan"><img src="img/3.jpeg" style="height: 165px; width: 250px;"></a>
                  <div class="grey-footer">
                    <h5> Teknik Kendaraan Ringan</h5>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-4 mb">
                <div class="grey-panel pn donut-chart">
                  <div class="grey-header">
                  <h5>Silahkan Klik</h5>
                  </div>
                  <a href="inven_admin.php?jurusan=teknik pengelasan"><img src="img/4.png" style="height: 165px; width: 250px;"></a>
                  <div class="grey-footer">
                    <h5> Teknik Pengelasan</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
  </section>
<?php
include "footer.php";
?>
</body>
</html>