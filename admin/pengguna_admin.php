<?php
include "header.php";
?>
<body>
  <section id="container">
    <header class="header black-bg" style="background: #365c7d;">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right"></div>
      </div>
      <a href="#" class="logo"><b>VOS<span>IN !</span></b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
           <li><a class="logout" href="../login/logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse " style="background: #303b58;">
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="img/8.png" class="img-circle" width="80"></a></p>
          <h5 class="centered">Admin !</h5>
          <li class="sub-menu">
            <a  href="javascript:;">
              <i class="fa fa-edit"></i>
              <span>Inventaris</span>
              </a>
            <ul class="sub">
              <li style="background: #303b58;"><a href="inventaris_admin.php">Data Inventaris</a></li>
              <li style="background: #303b58;"><a href="jenis_admin.php">Data Jenis</a></li>
              <li style="background: #303b58;"><a href="ruang_admin.php">Data Ruang</a></li>
            </ul>
          </li>
          <li>
                <a href="pinjam_admin.php">
                  <i class="fa fa-reply"></i>
                  <span>Data Peminjaman</span>
                </a>
              </li>
          <li>
            <a href="kembali_admin.php">
              <i class="fa fa-share"></i>
              <span>Data Pengembalian</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-cogs"></i>
              <span>Generate Laporan</span>
              </a>
            <ul class="sub">
              <li style="background: #303b58;"><a href="backup_database.php">Backup Database</a></li>
            </ul>
          </li>
          <li>
            <a class="active" href="pengguna_admin.php">
              <i class="fa fa-users"></i>
              <span>Pengguna </span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
    <section id="main-content">
      <section class="wrapper">
        <h3><i class="fa fa-angle-right"></i> Data Pengguna</h3>
        <div class="row mb">
          <div class="content-panel">
            <div class="container">
              <div class="adv-table">
              <a href="tambah_pengguna.php"><button class="btn btn-info"><i class="fa fa-plus"></i>ADD</button></a>
              <a href="lap_pengguna.php"><button class="btn btn-info"><i class="fa fa-plus"></i>Export PDF</button></a>
              <a href="export_excel_pengguna.php"><button class="btn btn-info"><i class="fa fa-plus"></i>Export Excel</button></a><br/><br/>
              <table class="display table table-bordered" id="hidden-table-info">
                <thead>
                  <tr align="center">
                    <td><b>No</b></td>
                    <td><b>Nama Member</b></td>
                    <td><b>Nip</b></td>
                    <td><b>Telp</b></td>
                    <td><b>Alamat</b></td>
                    <td><b>Option</b></td>
                  </tr>
                </thead>
               <tbody>
            <?php
                    include "../koneksi.php";
                    $no=1;
                    $sql = mysqli_query($koneksi, "SELECT * from member");
                    while ($s = mysqli_fetch_array($sql)){
            ?>
                <tr align="center">
                    <td height="42"><?php echo $no++;?></td>
                    <td><?=$s['nama_member'];?></td>
                    <td><?=$s['nip'];?></td>
                    <td><?=$s['telp'];?></td>
                    <td><?=$s['alamat'];?></td>
                    <td align="center">
                       <a href="edit_pengguna.php?id_member=<?=$s['id_member'];?>"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                    </td>
                 </tr>
                 <?php
               }
               ?>
            </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      </section>
    </section>
    <?php
      include "footer.php";
    ?>
    <!--footer end-->
  </section>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="lib/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="lib/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript">
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
      sOut += '<tr><td>Rendering engine:</td><td>' + aData[1] + ' ' + aData[4] + '</td></tr>';
      sOut += '<tr><td>Link to source:</td><td>Could provide a link here</td></tr>';
      sOut += '<tr><td>Extra info:</td><td>And any further details here (images etc)</td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
      nCloneTd.innerHTML = '<img src="lib/advanced-datatable/images/details_open.png">';
      nCloneTd.className = "center";

      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = "lib/advanced-datatable/media/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = "lib/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
  </script>
</body>

</html>
