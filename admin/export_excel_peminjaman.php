<!DOCTYPE html>
<html>
<head>
  <title>Vocabulary School Inventaris</title>
</head>
<body>
  <style type="text/css">
  body{
    font-family: sans-serif;
  }
  table{
    margin: 20px auto;
    border-collapse: collapse;
  }
  table th,
  table td{
    border: 1px solid #3c3c3c;
    padding: 3px 8px;

  }
  a{
    background: blue;
    color: #fff;
    padding: 8px 10px;
    text-decoration: none;
    border-radius: 2px;
  }
  </style>

<?php
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Data Peminjaman.xls");
?>

<center>
  <h1>Data Peminjaman</h1>
</center>

<table border="1">
  <thead>
    <tr>
        <td><b>No</b></td>
        <td><b>Nama Peminjaman</b></td>
        <td><b>Nama Barang</b></td>
        <td><b>Tanggal Pinjam</b></td>
        <td><b>Tanggal Kembali</b></td>
        <td><b>Jumlah</b></td>
    </tr>
  </thead>
                                    
 <tbody>
            <?php
                    include "../koneksi.php";
                    $no=1;
                    $select = mysqli_query($koneksi,  "SELECT * from peminjaman p JOIN detail_pinjam d ON p.id_peminjaman=d.id_peminjaman JOIN member m ON p.id_member=m.id_member JOIN inventaris i ON d.id_inventaris=i.id_inventaris");
                    while ($s = mysqli_fetch_array($select)){
            ?>
                <tr align="center">
                    <td height="42"><?php echo $no++;?></td>
                    <td><?=$s['nama_member'];?></td>
                    <td><?=$s['nama'];?></td>
                    <td><?=$s['tanggal_pinjam'];?></td>
                    <td><?=$s['tanggal_kembali'];?></td>
                    <td><?=$s['jumlah'];?></td>
                </tr>
                <?php
                }
                ?>
            </tbody>
</table>
                                 
</body>
</html>