<!DOCTYPE html>
<html>
<head>
  <title>Vocabulary School Inventaris</title>
</head>
<body>
  <style type="text/css">
  body{
    font-family: sans-serif;
  }
  table{
    margin: 20px auto;
    border-collapse: collapse;
  }
  table th,
  table td{
    border: 1px solid #3c3c3c;
    padding: 3px 8px;

  }
  a{
    background: blue;
    color: #fff;
    padding: 8px 10px;
    text-decoration: none;
    border-radius: 2px;
  }
  </style>

<?php
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Data Ruang.xls");
?>

<center>
  <h1>Data </h1>
</center>

<table border="1">
  <thead>
    <tr>
      <td><b>No</b></td>
      <td><b>Nama Ruang</b></td>
      <td><b>Kode Ruang</b></td>
      <td><b>Keterangan</b></td>
    </tr>
  </thead>
                                    
  <tbody>
            <?php
                    include "../koneksi.php";
                    $no=1;
                    $select = mysqli_query($koneksi, "SELECT * from ruang");
                    while ($w = mysqli_fetch_array($select)){
            ?>
                <tr align="center">
                    <td height="42"><?php echo $no++;?></td>
                    <td><?=$w['nama_ruang'];?></td>
                    <td><?=$w['kode_ruang'];?></td>
                    <td><?=$w['keterangan'];?></td>
                </tr>
                <?php
                }
                ?>
            </tbody>
</table>
                                 
</body>
</html>