<?php
include 'header.php';
?>
<body>
  <section id="container">
    <header class="header black-bg" style="background: #365c7d;">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right"></div>
      </div>
      <a href="#" class="logo"><b>VOS<span>IN !</span></b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="../login/logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse " style="background: #303b58;">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="img/8.png" class="img-circle" width="80"></a></p>
          <h5 class="centered">Admin !</h5>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-edit"></i>
              <span>Inventaris</span>
            </a>
            <ul class="sub">
              <li style="background: #303b58;"><a href="inventaris_admin.php">Data Inventaris</a></li>
              <li style="background: #303b58;"><a href="jenis_admin.php">Data Jenis</a></li>
              <li style="background: #303b58;"><a href="ruang_admin.php">Data Ruang</a></li>
            </ul>
          </li>
          <li>
            <a href="pinjam_admin.php">
              <i class="fa fa-reply"></i>
              <span>Data Peminjaman</span>
            </a>
          </li>
          <li>
            <a href="kembali_admin.php">
              <i class="fa fa-share"></i>
              <span>Data Pengembalian</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-cogs"></i>
              <span>Generate Laporan</span>
            </a>
            <ul class="sub">
              <li><a href="backup_database.php">Backup Database</a></li>
            </ul>
          </li>
          <li>
            <a class="active" href="pengguna_admin.php">
              <i class="fa fa-users"></i>
              <span>Pengguna </span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="container">
            <div class="col-md-10 col-sm-10 col-xs-10">
              <h3><i class="fa fa-angle-right"></i> Tambah Data Member</h3>
              <div class="x_content"><br>
            <form action="prossestambah_pengguna.php" method="post" class="form-horizontal form-label-left" novalidate>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_member">Nama Member</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" class="form-control" id="nama_member" placeholder="Masukan Nama Member" name="nama_member" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nip">NIP</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" class="form-control" id="nip" placeholder="Masukan NIP" name="nip" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telp">Telp</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" class="form-control" id="telp" placeholder="Masukan Telp" name="telp" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat</label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" class="form-control" id="alamat" placeholder="Masukan Alamat" name="alamat" required="">
                    </div>
                </div><br>
                <div class="ln_solid"></div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <a href="pengguna_admin.php" class="btn btn-primary" type="button">Cancel</a>
                        <button type="submit" class="btn btn-succes" name="simpan">Submit</button>
                    </div>
                </div>
                </form>

                  </div>
                </div>
              </div>
            </div>
          </section>
        </section>
      </section>
      <?php
      include "footer.php";
      ?>
    </body>

    </html>
