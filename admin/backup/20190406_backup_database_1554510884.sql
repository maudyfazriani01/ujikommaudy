DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `id_peminjaman` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(20) NOT NULL,
  `status` enum('Dikembalikan','Belum Dikembalikan') NOT NULL,
  PRIMARY KEY (`id_detail_peminjaman`),
  KEY `id_peminjaman` (`id_peminjaman`,`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("27","26","43","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("28","26","39","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("29","26","44","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("30","26","44","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("31","27","46","1","Dikembalikan");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kondisi` enum('Baik','Rusak Ringan','Rusak Berat','') NOT NULL,
  `foto` text NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`,`id_ruang`,`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("39","Mouse","Baik","","","9","19","0000-00-00","21","V0004","1");
INSERT INTO inventaris VALUES("43","Speaker ","Baik","","","2","19","0000-00-00","21","V0008","1");
INSERT INTO inventaris VALUES("44","CD","Baik","","","9","19","0000-00-00","21","V0009","1");
INSERT INTO inventaris VALUES("45","Earphone","Baik","8.png","","4","19","0000-00-00","21","V0010","1");
INSERT INTO inventaris VALUES("46","Laptop","Baik","7.png","","35","19","0000-00-00","21","V0011","1");
INSERT INTO inventaris VALUES("51","Kunci Inggris","Rusak Ringan","3.jpeg","","2","20","2019-03-02","16","V0015","1");
INSERT INTO inventaris VALUES("52","Laptop Acer","Baik","gue.png","","36","17","2018-03-03","18","V0016","1");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(11) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("17","Animasi","J0001","Ada");
INSERT INTO jenis VALUES("18","BroadCasting","J0002","Ada");
INSERT INTO jenis VALUES("19","Rekayasa Perangkat Lunak","J0003","Ada");
INSERT INTO jenis VALUES("20","Teknik Kendaraan Ringan","J0004","Ada");
INSERT INTO jenis VALUES("21","Teknik Pengelasan","J0005","Ada");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(15) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","user");



DROP TABLE member;

CREATE TABLE `member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `nama_member` varchar(50) NOT NULL,
  `nip` int(11) NOT NULL,
  `telp` int(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  PRIMARY KEY (`id_member`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO member VALUES("1","Maudy Fitri  Fazriani","117628672","896113969","Jln.Arde Laladon Indah");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(20) NOT NULL,
  `id_member` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_petugas` (`id_member`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("1","578","2019-03-14","2019-03-21","Belum Kembali","689");
INSERT INTO peminjaman VALUES("2","37","0000-00-00","0000-00-00","Sedang Dipinjam","0");
INSERT INTO peminjaman VALUES("3","36","0000-00-00","0000-00-00","Sedang Dipinjam","0");
INSERT INTO peminjaman VALUES("4","36","0000-00-00","0000-00-00","Sedang Dipinjam","0");
INSERT INTO peminjaman VALUES("5","36","0000-00-00","0000-00-00","Sedang Dipinjam","0");
INSERT INTO peminjaman VALUES("6","37","0000-00-00","2019-04-02","Telah Dikembalikan","1");
INSERT INTO peminjaman VALUES("20","39","2019-04-04","0000-00-00","Sedang Dipinjam","1");
INSERT INTO peminjaman VALUES("21","39","2019-04-04","0000-00-00","Sedang Dipinjam","1");
INSERT INTO peminjaman VALUES("22","39","2019-04-04","2019-04-04","Telah Dikembalikan","1");
INSERT INTO peminjaman VALUES("23","39","2019-04-04","2019-04-04","Telah Dikembalikan","1");
INSERT INTO peminjaman VALUES("24","44","2019-04-04","2019-04-04","Telah Dikembalikan","1");
INSERT INTO peminjaman VALUES("25","44","2019-04-04","2019-04-04","Telah Dikembalikan","1");
INSERT INTO peminjaman VALUES("26","43","2019-04-04","2019-04-04","Telah Dikembalikan","1");
INSERT INTO peminjaman VALUES("27","46","2019-04-05","0000-00-00","Sedang Dipinjam","0");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `email` varchar(35) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  `baned` enum('No','Yes') NOT NULL,
  `logintime` int(2) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin","maudyfazriani01@gmail.com","admin","maudy fitri","1","Yes","16");
INSERT INTO petugas VALUES("2","operator","maudyfazriani01@gmail.com","operator","maudy fitri","2","","0");
INSERT INTO petugas VALUES("3","user","maudyfazriani01@gmail.com","user","maudy fitri","3","","0");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(11) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("16","Bengkel TKu","R0001","Dekat Studio Animasi");
INSERT INTO ruang VALUES("17","Bengkel TPL","R0002","Dekat Tangga Sebelah Tata Usah");
INSERT INTO ruang VALUES("18","Lab Animasi","R0003","Sebelah Ruang Produksi");
INSERT INTO ruang VALUES("19","Lab Bahasa","R0004","Sebelah Tata Usaha");
INSERT INTO ruang VALUES("20","Lab RPL 1","R0005","Dekat Tangga Sebelah Utara");
INSERT INTO ruang VALUES("21","Lab RPL 2","R0006","Sebelah Lab RPL 1");
INSERT INTO ruang VALUES("22","Ruang Guru","R0007","Sebelah Mushola");
INSERT INTO ruang VALUES("23","Ruang Produksi","R0008","Sebelah Lab Animasi");
INSERT INTO ruang VALUES("24","Studio Animasi","R0009","Dekat Bengkel TKR ");
INSERT INTO ruang VALUES("25","Tata Usaha","R0010","Sebelah Lab Bahasa");
INSERT INTO ruang VALUES("26","Perpustakaan","R0011","Diatas Bengkel TPL");



