DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `id_peminjaman` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `status` enum('Dikembalikan','Belum Dikembalikan') NOT NULL,
  PRIMARY KEY (`id_detail_peminjaman`),
  KEY `id_peminjaman` (`id_peminjaman`,`id_inventaris`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kondisi` enum('Baik','Rusak Ringan','Rusak Berat','') NOT NULL,
  `spesifikasi` text NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `sumber` text NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`,`id_ruang`,`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("23","Crystal","Baik","","Ada","23","18","2019-02-28","10","V0001","1","");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(11) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("17","Alat Las","J0001","Ada");
INSERT INTO jenis VALUES("18","Elektronik","J0002","Ada");
INSERT INTO jenis VALUES("19","Alat Mobil","J0003","Ada");
INSERT INTO jenis VALUES("20","Alat Anim","J0004","Ada");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(15) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");



DROP TABLE member;

CREATE TABLE `member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `nama_member` varchar(50) NOT NULL,
  `nip` int(11) NOT NULL,
  `telp` int(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  PRIMARY KEY (`id_member`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO member VALUES("1","Maudy Fitri  Fazriani","117628672","896113969","Jln.Arde Laladon Indah");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(20) NOT NULL,
  `id_member` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_petugas` (`id_member`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("1","578","2019-03-14","2019-03-21","Belum Kembali","689");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `email` varchar(35) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  `baned` enum('No','Yes') NOT NULL,
  `logintime` int(2) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin","maudyfazriani01@gmail.com","admin","maudy fitri","1","Yes","16");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(11) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("8","LAB RPL 1","R0001","Dekat Ruang Pak Kartanto");
INSERT INTO ruang VALUES("9","LAB RPL 2","R0002","Sebelah Mushola");
INSERT INTO ruang VALUES("10","LAB RPL 3","R0003","Belum ada tempatnya");



