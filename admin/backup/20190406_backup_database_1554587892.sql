DROP TABLE detail_pinjam;

CREATE TABLE `detail_pinjam` (
  `id_detail_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `id_peminjaman` int(11) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah_pinjam` int(20) NOT NULL,
  `status` enum('Dikembalikan','Belum Dikembalikan') NOT NULL,
  PRIMARY KEY (`id_detail_peminjaman`),
  KEY `id_peminjaman` (`id_peminjaman`,`id_inventaris`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

INSERT INTO detail_pinjam VALUES("27","26","43","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("28","26","39","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("29","26","44","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("30","26","44","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("31","27","46","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("32","28","52","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("33","29","52","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("34","30","52","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("35","31","52","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("36","32","52","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("37","32","52","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("38","32","52","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("39","33","52","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("40","34","52","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("41","35","52","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("42","36","52","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("43","37","46","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("44","38","46","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("45","39","45","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("46","40","53","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("47","41","53","2","Dikembalikan");
INSERT INTO detail_pinjam VALUES("48","42","53","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("49","43","46","1","Dikembalikan");
INSERT INTO detail_pinjam VALUES("50","44","46","1","Dikembalikan");



DROP TABLE inventaris;

CREATE TABLE `inventaris` (
  `id_inventaris` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `kondisi` enum('Baik','Rusak Ringan','Rusak Berat','') NOT NULL,
  `foto` text NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `tanggal_register` date NOT NULL,
  `id_ruang` int(11) NOT NULL,
  `kode_inventaris` varchar(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  PRIMARY KEY (`id_inventaris`),
  KEY `id_jenis` (`id_jenis`,`id_ruang`,`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

INSERT INTO inventaris VALUES("39","Mouse","Baik","","","9","19","0000-00-00","21","V0004","1");
INSERT INTO inventaris VALUES("43","Speaker ","Baik","","","2","19","0000-00-00","21","V0008","1");
INSERT INTO inventaris VALUES("44","CD","Baik","","","9","19","0000-00-00","21","V0009","1");
INSERT INTO inventaris VALUES("45","Earphone","Baik","8.png","","3","19","0000-00-00","21","V0010","1");
INSERT INTO inventaris VALUES("46","Laptop","Baik","7.png","","33","19","0000-00-00","21","V0011","1");
INSERT INTO inventaris VALUES("51","Kunci Inggris","Rusak Ringan","3.jpeg","","2","20","2019-03-02","16","V0015","1");
INSERT INTO inventaris VALUES("52","Laptop Acer","Baik","gue.png","","26","17","2018-03-03","18","V0016","1");
INSERT INTO inventaris VALUES("53","Kacamata Las","Baik","tkr.png","","28","21","2017-09-08","17","V0017","1");
INSERT INTO inventaris VALUES("54","Kamera","Baik","7.png","","3","18","2019-01-08","24","V0018","1");



DROP TABLE jenis;

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis` varchar(50) NOT NULL,
  `kode_jenis` varchar(11) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

INSERT INTO jenis VALUES("17","Animasi","J0001","Ada");
INSERT INTO jenis VALUES("18","BroadCasting","J0002","Ada");
INSERT INTO jenis VALUES("19","Rekayasa Perangkat Lunak","J0003","Ada");
INSERT INTO jenis VALUES("20","Teknik Kendaraan Ringan","J0004","Ada");
INSERT INTO jenis VALUES("21","Teknik Pengelasan","J0005","Ada");



DROP TABLE level;

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL AUTO_INCREMENT,
  `nama_level` varchar(15) NOT NULL,
  PRIMARY KEY (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO level VALUES("1","admin");
INSERT INTO level VALUES("2","operator");
INSERT INTO level VALUES("3","user");



DROP TABLE member;

CREATE TABLE `member` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `nama_member` varchar(50) NOT NULL,
  `nip` int(11) NOT NULL,
  `telp` int(15) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `status` varchar(200) NOT NULL,
  `user_key` int(200) NOT NULL,
  PRIMARY KEY (`id_member`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO member VALUES("1","Maudy Fitri  Fazriani","1122","896113969","Jln.Arde Laladon Indah","","0");
INSERT INTO member VALUES("2","Maudy Fazriani","1234","219634739","Ciomas","","0");
INSERT INTO member VALUES("3","Maudy Fitri","738798739","275872898","Laladon","","0");



DROP TABLE peminjaman;

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT,
  `id_inventaris` int(11) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `status_peminjaman` varchar(20) NOT NULL,
  `id_member` int(11) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  PRIMARY KEY (`id_peminjaman`),
  KEY `id_petugas` (`id_member`),
  KEY `id_petugas_2` (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

INSERT INTO peminjaman VALUES("39","45","2019-04-06","0000-00-00","Sedang Dipinjam","1","0");
INSERT INTO peminjaman VALUES("40","53","2019-04-07","0000-00-00","Sedang Dipinjam","1","0");
INSERT INTO peminjaman VALUES("42","53","2019-04-07","0000-00-00","Sedang Dipinjam","2","0");
INSERT INTO peminjaman VALUES("44","46","2019-04-07","0000-00-00","Sedang Dipinjam","0","2");



DROP TABLE petugas;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `email` varchar(35) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_petugas` varchar(50) NOT NULL,
  `id_level` int(11) NOT NULL,
  `status` int(200) NOT NULL,
  `user_key` varchar(200) NOT NULL,
  PRIMARY KEY (`id_petugas`),
  KEY `id_level` (`id_level`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO petugas VALUES("1","admin","maudyfazriani01@gmail.com","21232f297a57a5a743894a0e4a801fc3","maudy fitri","1","1","efad8be464ac484880dcb00a2f7746d85ca86704696ca");
INSERT INTO petugas VALUES("2","operator","maudyfazriani01@gmail.com","4b583376b2767b923c3e1da60d10de59","maudy fitri","2","0","0");
INSERT INTO petugas VALUES("3","user","maudyfazriani01@gmail.com","user","maudy fitri","3","0","0");



DROP TABLE ruang;

CREATE TABLE `ruang` (
  `id_ruang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ruang` varchar(50) NOT NULL,
  `kode_ruang` varchar(11) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  PRIMARY KEY (`id_ruang`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

INSERT INTO ruang VALUES("16","Bengkel tkr","R0001","Dekat Studio Animasi");
INSERT INTO ruang VALUES("17","Bengkel TPL","R0002","Dekat Tangga Sebelah Tata Usah");
INSERT INTO ruang VALUES("18","Lab Animasi","R0003","Sebelah Ruang Produksi");
INSERT INTO ruang VALUES("19","Lab Bahasa","R0004","Sebelah Tata Usaha");
INSERT INTO ruang VALUES("20","Lab RPL 1","R0005","Dekat Tangga Sebelah Utara");
INSERT INTO ruang VALUES("21","Lab RPL 2","R0006","Sebelah Lab RPL 1");
INSERT INTO ruang VALUES("22","Ruang Guru","R0007","Sebelah Mushola");
INSERT INTO ruang VALUES("23","Ruang Produksi","R0008","Sebelah Lab Animasi");
INSERT INTO ruang VALUES("24","Studio Animasi","R0009","Dekat Bengkel TKR ");
INSERT INTO ruang VALUES("25","Tata Usaha","R0010","Sebelah Lab Bahasa");
INSERT INTO ruang VALUES("26","Perpustakaan","R0011","Diatas Bengkel TPL");



