<?php
include 'header.php';
?>
<body>
  <section id="container">
    <header class="header black-bg" style="background: #365c7d;">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right"></div>
      </div>
      <a href="#" class="logo"><b>VOS<span>IN !</span></b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="../login/logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse " style="background: #303b58;">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="img/8.png" class="img-circle" width="80"></a></p>
          <h5 class="centered">Admin !</h5>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-edit"></i>
              <span>Inventaris</span>
              </a>
            <ul class="sub">
              <li style="background: #303b58;"><a href="inventaris_admin.php">Data Inventaris</a></li>
              <li style="background: #303b58;"><a href="jenis_admin.php">Data Jenis</a></li>
              <li style="background: #303b58;"><a href="ruang_admin.php">Data Ruang</a></li>
            </ul>
          </li>
      <li>
            <a href="pinjam_admin.php">
              <i class="fa fa-reply"></i>
              <span>Data Peminjaman</span>
            </a>
          </li>
      <li>
            <a href="kembali_admin.php">
              <i class="fa fa-share"></i>
              <span>Data Pengembalian</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-cogs"></i>
              <span>Backup Database</span>
              </a>
            <ul class="sub">
              <li style="background: #303b58;"><a href="backup_database.php">Backup Database</a></li>
            </ul>
          </li>
          <li>
            <a href="pengguna_admin.php">
              <i class="fa fa-envelope"></i>
              <span>Pengguna </span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
    <section id="main-content">
      <section class="wrapper">
    <div class="container">
    <div class="row">
        <div class="col-md-10 col-sm-10 col-xs-10">
        <h3><i class="fa fa-angle-right"></i> Edit Data Ruang</h3>
<?php
    include "../koneksi.php";
    $id_ruang=$_GET['id_ruang'];
    $b = mysqli_query($koneksi,"SELECT * from ruang where id_ruang='$id_ruang'");
    $r = mysqli_fetch_array($b);
?>
            <form method="POST">
                <div class="form-group">
                    <label for="inputnama_ruang">Nama Ruang</label>
                    <input type="text" name="nama_ruang" class="form-control" id="inputnama_ruang" placeholder="Nama Ruang" value="<?php echo $r['nama_ruang'];?>" required>
                </div>
                <div class="form-group">
                    <label for="inputkode_ruang">Kode Ruang</label>
                    <input type="text" name="kode_ruang" class="form-control" id="inputkode_ruang" placeholder="Kode Ruang" value="<?php echo $r['kode_ruang'];?>" required>
                </div>
                <div class="form-group">
                    <label for="inputketerangan">Keterangan</label>
                    <input type="text" name="keterangan" class="form-control" id="inputketerangan" placeholder="Keterangan" value="<?php echo $r['keterangan'];?>" required>
                </div>
                <div class="ln_solid"></div>
                <button type="submit" class="btn btn-info" name="edit">Submit</button>
            </form>
<?php

    if(isset($_POST['edit'])){

    $nama_ruang =   $_POST['nama_ruang'];
    $kode_ruang =   $_POST['kode_ruang'];
    $keterangan =   $_POST['keterangan'];

    
    $sql=  mysqli_query($koneksi,"UPDATE ruang set nama_ruang='$nama_ruang', kode_ruang='$kode_ruang', keterangan='$keterangan' WHERE id_ruang='$_GET[id_ruang]'");
    if($sql){
      echo"<script>window.location.assign('ruang_admin.php')</script>";
    }else{
      echo"Gagal";
    }

}
?>
    
        </div>
    </div>
    </div>
      </section>
    </section>
  </section>
<?php
include "footer.php";
?>
</body>

</html>
