<?php
$koneksi = new PDO("mysql:host=localhost;dbname=ujikom_maudy", "root", "");
$get_all_table_query = "SHOW TABLES";
$statement = $koneksi->prepare($get_all_table_query);
$statement->execute();
$result = $statement->fetchAll();

if(isset($_POST['table']))
{
 $output = '';
 foreach($_POST["table"] as $table)
 {
  $show_table_query = "SHOW CREATE TABLE " . $table . "";
  $statement = $koneksi->prepare($show_table_query);
  $statement->execute();
  $show_table_result = $statement->fetchAll();

  foreach($show_table_result as $show_table_row)
  {
   $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
 }
 $select_query = "SELECT * FROM " . $table . "";
 $statement = $koneksi->prepare($select_query);
 $statement->execute();
 $total_row = $statement->rowCount();

 for($count=0; $count<$total_row; $count++)
 {
   $single_result = $statement->fetch(PDO::FETCH_ASSOC);
   $table_column_array = array_keys($single_result);
   $table_value_array = array_values($single_result);
   $output .= "\nINSERT INTO $table (";
   $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
   $output .= "'" . implode("','", $table_value_array) . "');\n";
 }
}
$file_name = 'inventaris_smk_' . date('y-m-d') . '.sql';
$file_handle = fopen($file_name, 'w+');
fwrite($file_handle, $output);
fclose($file_handle);
header('Content-Description: File Transfer');
header('Content-Type: application/octet-stream');
header('Content-Disposition: attachment; filename=' . basename($file_name));
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($file_name));
ob_clean();
flush();
readfile($file_name);
unlink($file_name);
}

?>

<?php
include 'header.php';
?>
<body>
  <section id="container">
    <header class="header black-bg" style="background: #365c7d;">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right"></div>
      </div>
      <a href="#" class="logo"><b>VOS<span>IN !</span></b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="../login/logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse " style="background: #303b58;">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="img/8.png" class="img-circle" width="80"></a></p>
          <h5 class="centered">Admin !</h5>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-edit"></i>
              <span>Inventaris</span>
            </a>
            <ul class="sub">
              <li style="background: #303b58;"><a href="inven_admin.php">Data Inventaris</a></li>
              <li style="background: #303b58;"><a href="jenis_admin.php">Data Jenis</a></li>
              <li style="background: #303b58;"><a href="ruang_admin.php">Data Ruang</a></li>
            </ul>
          </li>
          <li>
            <a href="pinjam_admin.php">
              <i class="fa fa-reply"></i>
              <span>Data Peminjaman</span>
            </a>
          </li>
          <li>
            <a href="kembali_admin.php">
              <i class="fa fa-share"></i>
              <span>Data Pengembalian</span>
            </a>
          </li>
          <li class="sub-menu">
            <a class="active" href="javascript:;">
              <i class="fa fa-cogs"></i>
              <span>Backup Database</span>
            </a>
            <ul class="sub">
              <li style="background: #303b58;"><a href="backup_database.php">Backup Database</a></li>
            </ul>
          </li>
          <li>
            <a href="pengguna_admin.php">
              <i class="fa fa-users"></i>
              <span>Pengguna </span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2><i class="fa fa-angle-right"></i>Back Up Database</h2>                                    
                  <div class="clearfix"></div>
                </div>
                <div class="content-panel">
                  <div class="modal-body">
                    <h3>Backup Database </h3>
                    <p>Data kamu sudah penuh!! ini saat nya kamu untuk backup database.</p>
                    <div class="panel-body">
                      <?php
                      error_reporting(0);
                      $file=date("Ymd").'_backup_database_'.time().'.sql';
                      backup_tables("localhost","root","","ujikom_maudy",$file);
                      ?>
                      <div class="form-group pull-right">
                        <a style="cursor:pointer" onclick="location.href='download_backup_data.php?nama_file=<?php echo $file;?>'" title="Download" class="icon-layers" >&nbsp;Backup</a>
                      </div> 
                      <?php
                        /*
                        untuk memanggil nama fungsi :: jika anda ingin membackup semua tabel yang ada didalam database, biarkan tanda BINTANG (*) pada variabel $tables = '*'
                        jika hanya tabel-table tertentu, masukan nama table dipisahkan dengan tanda KOMA (,) 
                        */
                        function backup_tables($host,$user,$pass,$name,$nama_file,$tables ='*') {
                          $link = mysql_connect($host,$user,$pass);
                          mysql_select_db($name,$link);
                          
                          if($tables == '*'){
                            $tables = array();
                            $result = mysql_query('SHOW TABLES');
                            while($row = mysql_fetch_row($result)){
                              $tables[] = $row[0];
                            }
                          }
                          else{//jika hanya table-table tertentu
                            $tables = is_array($tables) ? $tables : explode(',',$tables);
                          }
                          
                          foreach($tables as $table){
                            $result = mysql_query('SELECT * FROM '.$table);
                            $num_fields = mysql_num_fields($result);

                            $return.= 'DROP TABLE '.$table.';';//menyisipkan query drop table untuk nanti hapus table yang lama
                            $row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
                            $return.= "\n\n".$row2[1].";\n\n";
                            
                            for ($i = 0; $i < $num_fields; $i++) {
                              while($row = mysql_fetch_row($result)){
                                //menyisipkan query Insert. untuk nanti memasukan data yang lama ketable yang baru dibuat
                                $return.= 'INSERT INTO '.$table.' VALUES(';
                                for($j=0; $j<$num_fields; $j++) {
                                  //akan menelusuri setiap baris query didalam
                                  $row[$j] = addslashes($row[$j]);
                                  $row[$j] = ereg_replace("\n","\\n",$row[$j]);
                                  if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                                  if ($j<($num_fields-1)) { $return.= ','; }
                                }
                                $return.= ");\n";
                              }
                            }
                            $return.="\n\n\n";
                          }             
                          //simpan file di folder
                          $nama_file;
                          
                          $handle = fopen('backup/'.$nama_file,'w+');
                          fwrite($handle,$return);
                          fclose($handle);
                        }
                        ?>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-angle-right"></i>Back Up Database</h2>                                    
                    <div class="clearfix"></div>
                  </div>
                  <center>
                    <div class="content-panel">
                      <form method="post" id="export_form" >
                        <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pilih Tabel Untuk Export</h3>
                        <?php
                        foreach($result as $table)
                        {
                          ?>

                          <div class="checkbox">
                            <label><input type="checkbox" class="checkbox_table" name="table[]" value="<?php echo $table["Tables_in_ujikom_maudy"]; ?>" /> <?php echo $table["Tables_in_ujikom_maudy"]; ?></label>
                          </div>
                          <?php
                        }
                        ?>
                        <div class="form-group">
                          <input type="submit" name="submit" id="submit" class="btn btn-info" value="Export" />
                        </div>
                      </form>

                    </div>
                  </center>
                </div>
              </div>

            </div>
          </div>
        </section>
      </section>
    </section>
    <?php
    include "footer.php";
    ?>
  </body>

  </html>
