<?php
include 'header.php';
?>
<body>
  <section id="container">
    <header class="header black-bg" style="background: #365c7d;">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right"></div>
      </div>
      <a href="#" class="logo"><b>VOS<span>IN !</span></b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="../login/logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse " style="background: #303b58;">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="img/8.png" class="img-circle" width="80"></a></p>
          <h5 class="centered">Admin !</h5>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-edit"></i>
              <span>Inventaris</span>
              </a>
            <ul class="sub">
              <li style="background: #303b58;"><a href="inventaris_admin.php">Data Inventaris</a></li>
              <li style="background: #303b58;"><a href="jenis_admin.php">Data Jenis</a></li>
              <li style="background: #303b58;"><a href="ruang_admin.php">Data Ruang</a></li>
            </ul>
          </li>
      <li>
            <a href="pinjam_admin.php">
              <i class="fa fa-reply"></i>
              <span>Data Peminjaman</span>
            </a>
          </li>
      <li>
            <a href="kembali_admin.php">
              <i class="fa fa-share"></i>
              <span>Data Pengembalian</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-cogs"></i>
              <span>Generate Laporan</span>
              </a>
            <ul class="sub">
              <li style="background: #303b58;"><a href="backup_database.php">Backup Database</a></li>
            </ul>
          </li>
          <li>
            <a class="active" href="pengguna_admin.php">
              <i class="fa fa-users"></i>
              <span>Pengguna </span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
    <section id="main-content">
      <section class="wrapper">
    <div class="container">
    <div class="row">
        <div class="col-md-10 col-sm-10 col-xs-10">
        <h3><i class="fa fa-angle-right"></i> Edit Data Member</h3>
<?php
    include "../koneksi.php";
    $id_member=$_GET['id_member'];
    $b = mysqli_query($koneksi,"SELECT * from member where id_member='$id_member'");
    $r = mysqli_fetch_array($b);
?>
           <form method="POST">
                <div class="form-group">
                    <label for="input">Nama Member</label>
                    <input type="text" name="nama_member" class="form-control" id="inputnama_member" placeholder="Nama Member" value="<?php echo $r['nama_member'];?>" required>
                </div>
                <div class="form-group">
                    <label for="input">NIP</label>
                    <input type="text" name="nip" class="form-control" id="inputnip" placeholder="NIP" value="<?php echo $r['nip'];?>" required>
                </div>
                <div class="form-group">
                    <label for="input">Telp</label>
                    <input type="text" name="telp" class="form-control" id="inputtelp" placeholder="Telp" value="<?php echo $r['telp'];?>" required>
                </div>
                <div class="form-group">
                    <label for="input">Alamat</label>
                    <input type="text" name="alamat" class="form-control" id="inputalamat" placeholder="Alamat" value="<?php echo $r['alamat'];?>" required>
                </div>
                <button type="submit" class="btn btn-info" name="edit">Submit</button>
            </form>
<?php

    if(isset($_POST['edit'])){

    $nama_member =   $_POST['nama_member'];
    $nip =   $_POST['nip'];
    $telp =   $_POST['telp'];
    $alamat =   $_POST['alamat'];

    
    $sql=  mysqli_query($koneksi,"UPDATE member set nama_member='$nama_member', nip='$nip', telp='$telp', alamat='$alamat' WHERE id_member='$_GET[id_member]'");
    if($sql){
      echo"Berhasil";
      echo"<script>window.location.assign('pengguna_admin.php')</script>";
    }else{
      echo"Gagal";
    }

}
?>
    
        </div>
    </div>
    </div>
      </section>
    </section>
  </section>
<?php
include "footer.php";
?>
</body>

</html>
