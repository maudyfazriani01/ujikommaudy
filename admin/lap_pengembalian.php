<?php
include '../koneksi.php';
include 'pdf/fpdf.php';

$pdf = new FPDF("L","cm","A4");

$pdf->SetMargins(2,1,1);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','B',11);
$pdf->Image('img/index.jpg',1,1,2,2);
$pdf->SetX(4);            
$pdf->MultiCell(19.5,0.5,'SMKN 1 CIOMAS',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'Telpon : 0038XXXXXXX',0,'L');    
$pdf->SetFont('Arial','B',10);
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'JL. RAYA LALADON INDAH',0,'L');
$pdf->SetX(4);
$pdf->MultiCell(19.5,0.5,'website : www.smkn1ciomas.com email : smkn1ciomas@gmail.com',0,'L');
$pdf->Line(1,3.1,28.5,3.1);
$pdf->SetLineWidth(0.1);      
$pdf->Line(1,3.2,28.5,3.2);   
$pdf->SetLineWidth(0);
$pdf->ln(1);
$pdf->SetFont('Arial','B',14);
$pdf->Cell(25.5,0.7,"Laporan Data Peminjaman",0,10,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(5,0.7,"Di cetak pada : ".date("D-d/m/Y"),0,0,'C');
$pdf->ln(1);
$pdf->SetFont('Arial','B',10);
$pdf->Cell(2, 0.8, 'NO', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'Nama Peminjam', 1, 0, 'C');
$pdf->Cell(6, 0.8, 'Nama Barang', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tanggal Pinjam', 1, 0, 'C');
$pdf->Cell(4, 0.8, 'Tanggal Kembali', 1, 0, 'C');
$pdf->Cell(3, 0.8, 'Jumlah', 1, 1, 'C');

$pdf->SetFont('Arial','',10);
$no=1;
$query=mysqli_query($koneksi,"SELECT * from peminjaman p JOIN detail_pinjam d ON p.id_peminjaman=d.id_peminjaman JOIN petugas t ON p.id_petugas=t.id_petugas JOIN inventaris i ON d.id_inventaris=i.id_inventaris");
while($lihat=mysqli_fetch_array($query)){
	$pdf->Cell(2, 0.8, $no , 1, 0, 'C');
	$pdf->Cell(6, 0.8, $lihat['username'],1, 0, 'C');
	$pdf->Cell(6, 0.8, $lihat['nama'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['tanggal_pinjam'], 1, 0,'C');
	$pdf->Cell(4, 0.8, $lihat['tanggal_kembali'], 1, 0,'C');
	$pdf->Cell(3, 0.8, $lihat['jumlah'],1, 1, 'C');


	$no++;
}

$pdf->Output("laporan_ruang.pdf","I");

?>


