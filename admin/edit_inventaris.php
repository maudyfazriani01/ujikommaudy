<?php
include 'header.php';
?>
<body>
  <section id="container">
    <header class="header black-bg" style="background: #365c7d;">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right"></div>
      </div>
      <a href="#" class="logo"><b>VOS<span>IN !</span></b></a>
      <div class="top-menu">
        <ul class="nav pull-right top-menu">
          <li><a class="logout" href="../login/logout.php">Logout</a></li>
        </ul>
      </div>
    </header>
    <aside>
      <div id="sidebar" class="nav-collapse " style="background: #303b58;">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
          <p class="centered"><a href="profile.html"><img src="img/8.png" class="img-circle" width="80"></a></p>
          <h5 class="centered">Admin !</h5>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-edit"></i>
              <span>Inventaris</span>
              </a>
            <ul class="sub">
              <li style="background: #303b58;"><a href="inventaris_admin.php">Data Inventaris</a></li>
              <li style="background: #303b58;"><a href="jenis_admin.php">Data Jenis</a></li>
              <li style="background: #303b58;"><a href="ruang_admin.php">Data Ruang</a></li>
            </ul>
          </li>
      <li>
            <a href="pinjam_admin.php">
              <i class="fa fa-reply"></i>
              <span>Data Peminjaman</span>
            </a>
          </li>
      <li>
            <a href="kembali_admin.php">
              <i class="fa fa-share"></i>
              <span>Data Pengembalian</span>
            </a>
          </li>
          <li class="sub-menu">
            <a href="javascript:;">
              <i class="fa fa-cogs"></i>
              <span>Backup Database</span>
              </a>
            <ul class="sub">
              <li><a style="background: #303b58;" href="backup_database.php">Backup Database</a></li>
            </ul>
          </li>
          <li>
            <a href="pengguna_admin.php">
              <i class="fa fa-envelope"></i>
              <span>Pengguna </span>
            </a>
          </li>
        </ul>
      </div>
    </aside>
    <section id="main-content">
      <section class="wrapper">
    <div class="container">
    <div class="row">
        <div class="col-md-10 col-sm-10 col-xs-10">
        <h3><i class="fa fa-angle-right"></i> Edit Data Inventaris</h3>
<?php
    include "../koneksi.php";
    $id_inventaris=$_GET['id_inventaris'];
    $nama = mysqli_query($koneksi,"SELECT * from inventaris where id_inventaris='$id_inventaris'");
    $r = mysqli_fetch_array($nama);
?>
            <form method="POST">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama" value="<?php echo $r['nama'];?>">
                </div>
                <div class="form-group">
                    <label for="kondisi">Kondisi</label>
                    <input type="text" name="kondisi" class="form-control" id="kondisi" placeholder="Kondisi" value="<?php echo $r['kondisi'];?>">
                </div>
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <input type="text" name="keterangan" class="form-control" id="keterangan" placeholder="Keterangan" value="<?php echo $r['keterangan'];?>">
                </div>
                <div class="form-group">
                    <label for="jumlah">Jumlah</label>
                    <input type="text" name="jumlah" class="form-control" id="jumlah" placeholder="Jumlah" value="<?php echo $r['jumlah'];?>">
                </div>
                <div class="form-group">
                    <label for="id_jenis">Nama Jenis</label>
                    <input type="text" name="id_jenis" class="form-control" id="id_jenis" placeholder="Nama Jenis" value="<?php echo $r['id_jenis'];?>">
                </div>
                <div class="form-group">
                    <label for="tanggal_register">Tanggal Register</label>
                    <input type="date" name="tanggal_register" class="form-control" id="tanggal_register" placeholder="Tanggal Register" value="<?php echo $r['tanggal_register'];?>">
                </div>
                <div class="form-group">
                    <label for="id_ruang">Nama Ruang</label>
                    <input type="text" name="id_ruang" class="form-control" id="id_ruang" placeholder="Nama Ruang" value="<?php echo $r['id_ruang'];?>">
                </div>
                <div class="form-group">
                    <label for="kode_inventaris">Kode Inventaris</label>
                    <input type="text" name="kode_inventaris" class="form-control" id="kode_inventaris" placeholder="Kode Inventaris" value="<?php echo $r['kode_inventaris'];?>">
                </div>
                <div class="form-group">
                    <label for="id_petugas">Nama Petugas</label>
                    <input type="text" name="id_petugas" class="form-control" id="id_petugas" placeholder="Nama Petugas" value="<?php echo $r['id_petugas'];?>">
                </div>
                <div class="form-group">
                    <label for="foto">Foto</label>
                    <input type="file" name="foto" class="form-control" id="foto"  value="<?php echo $r['foto'];?>">
                </div>
                <button type="submit" class="btn btn-default" name="edit">Submit</button>
                <button class="btn btn-default"><a href ="inven_admin.php"> Kembali </a></button></td>
            </form>
<?php
    if(isset($_POST['edit'])){

    $nama           =   $_POST['nama'];
    $kondisi        =   $_POST['kondisi'];
    $keterangan     =   $_POST['keterangan'];
    $jumlah         =   $_POST['jumlah'];
    $id_jenis       =   $_POST['id_jenis'];
    $tanggal_register=  $_POST['tanggal_register'];
    $id_ruang       =   $_POST['id_ruang'];
    $kode_inventaris=   $_POST['kode_inventaris'];
    $id_petugas     =   $_POST['id_petugas'];
    $foto           =   $_POST['foto'];

    
    $sql=  mysqli_query($koneksi,"UPDATE inventaris set nama='$nama', kondisi='$kondisi', keterangan='$keterangan', jumlah='$jumlah', id_jenis='$id_jenis',tanggal_register='$tanggal_register', id_ruang='$id_ruang', kode_inventaris='$kode_inventaris', id_petugas='$id_petugas' , foto='$foto' where id_inventaris='$id_inventaris'");
    if($sql){
      echo"<script>window.location.assign('inven_admin.php');</script>";
    }else{
      echo"Gagal";
    }

}
?>
    
        </div>
    </div>
    </div>
      </section>
    </section>
  </section>
<?php
include "footer.php";
?>
</body>

</html>
